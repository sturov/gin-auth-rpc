package ginrmq

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/porthos-rpc/porthos-go/broker"
	"github.com/porthos-rpc/porthos-go/client"
	"github.com/porthos-rpc/porthos-go/status"
)

const HeaderAuthUser = "X-Auth-UserId"

type AuthMiddlewareRMQ struct {
	amqp string
}

type AuthenticateData struct {
	UserID string `json:"user_id"`
}

// NewAuthMiddlewareRMQ instance.
func NewAuthMiddlewareRMQ(amqp string) AuthMiddlewareRMQ {
	return AuthMiddlewareRMQ{amqp}
}

// Authorize middleware func.
func (r *AuthMiddlewareRMQ) Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		token, err := r.parseToken(c)
		if err != nil {
			respondWithError(c, http.StatusUnauthorized, err.Error())
			return
		}

		info, err := r.CallAuthorize(token)
		if err != nil {
			respondWithError(c, http.StatusUnauthorized, "Need authorization")
			return
		}

		c.Request.Header.Set(HeaderAuthUser, info.UserID)
		respondWithSuccess(c)
	}
}

func (r *AuthMiddlewareRMQ) TryAuthorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		token, err := r.parseToken(c)
		if err != nil {
			respondWithSuccess(c)
			return
		}

		info, err := r.CallAuthorize(token)
		if err == nil {
			c.Request.Header.Set(HeaderAuthUser, info.UserID)
			return
		}

		respondWithSuccess(c)
	}
}

func respondWithError(c *gin.Context, code int, message string) {
	c.JSON(http.StatusUnauthorized, gin.H{
		"code":    http.StatusUnauthorized,
		"message": message,
	})
	c.Abort()
}

func respondWithSuccess(c *gin.Context) {
	c.Next()
}

func (r *AuthMiddlewareRMQ) CallAuthorize(token string) (*AuthenticateData, error) {
	// first of all you need a broker
	b, err := broker.NewBroker(r.amqp)
	if err != nil {
		panic(err)
	}

	defer b.Close()

	// then you create a new client (you can have as many clients as you want using the same broker)
	authService, _ := client.NewClient(b, "AuthService", 120)
	defer authService.Close()

	// finally the remote call. It returns a response that contains the output channel.
	ret, _ := authService.Call("authorize-bearrer", token)
	defer ret.Dispose()

	select {
	case r := <-ret.ResponseChannel():
		if r.StatusCode == status.OK {
			info := &AuthenticateData{}
			r.UnmarshalJSONTo(info)
			return info, nil
		}
	case <-time.After(2 * time.Second):
		fmt.Println("Timed out :(")
	}
	return nil, errors.New("Error call.")
}

func (r *AuthMiddlewareRMQ) parseToken(c *gin.Context) (string, error) {
	authHeader := c.Request.Header.Get("Authorization")
	if authHeader == "" {
		return "", errors.New("Auth header empty")
	}
	parts := strings.SplitN(authHeader, " ", 2)
	if !(len(parts) == 2 && parts[0] == "Bearer") {
		return "", errors.New("Invalid auth header")
	}
	return parts[1], nil
}
